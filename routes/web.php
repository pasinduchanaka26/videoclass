<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', function () {
    return redirect(route('login'));
});

Auth::routes();

/*Route::get('/admin-dashboard', 'AdminController@index')->name('admin-dashboard');
Route::get('/student-dashboard', 'StudentController@index')->name('student-dashboard');*/
Route::resource('admin-dashboard', 'AdminController');
Route::resource('student-dashboard', 'StudentController');
Route::resource('video', 'VideoController');
