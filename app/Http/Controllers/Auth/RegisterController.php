<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Student;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = 'student-dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        //dd ($data);
       /* return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);*/
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'birth_date' => ['required'],
            'nic' => ['required','unique:students'],
            'guardian_name' => ['required', 'string', 'max:255'],
            'mobile_phone' => ['required'],
            'land_phone' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        try{
            $db_transaction=DB::transaction(function ()use($data) {

                $user=User::create([
                          'name' => $data['first_name']."".$data['last_name'],
                          'email' => $data['email'],
                          'password' => Hash::make($data['password']),
                      ]);
                $user->attachRole('student');
                $student=new Student();
                $student->user_id=$user->id;
                $student->first_name=$data['first_name'];
                $student->last_name=$data['last_name'];
                $student->birth_date=$data['birth_date'];
                $student->nic=$data['nic'];
                $student->contact_address=$data['contact_address'];
                $student->mobile_phone=$data['mobile_phone'];
                $student->land_phone=$data['land_phone'];
                $student->email=$data['email'];
                $student->guardian_name=$data['guardian_name'];
                $student->is_active=1;
                $student->is_delete=0;
                $student->save();

                return $user;

            });
            return $db_transaction;
        }catch(\Illuminate\Database\QueryException $e){
            throw $e;
        }
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);*/
    }
}
