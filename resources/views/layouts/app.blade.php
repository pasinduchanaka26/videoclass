<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.header')
</head>

<body class="">
<div class="wrapper ">
    @include('includes.left-sidebar')
    <div class="main-panel">
        <!-- Navbar -->
        @include('includes.top-bar')
        <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">

                @yield('content')

            </div>
        </div>
        @include('includes.footer')
    </div>
</div>
<!--   Core JS Files   -->
@include('includes.scripts')
@yield('script')
<!--  End Core JS Files   -->

</body>

</html>
