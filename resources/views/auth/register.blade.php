{{--@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection--}}

{{--<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.header')
</head>

<body class="">
<div class="wrapper ">
    <div class="main-panel">
        <!-- Navbar -->
    <!-- End Navbar -->
        <div class="content">
            <div class="container-fluid">
                <div class="container">
                    <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">{{ __('Student Register') }}</div>

                                <div class="card-body">
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="name" class="col-md-6 col-form-label text-md-right">{{ __('Name') }}</label>

                                            <div class="col-md-12">
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="email" class="col-md-6 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                            <div class="col-md-12">
                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password" class="col-md-6 col-form-label text-md-right">{{ __('Password') }}</label>

                                            <div class="col-md-12">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password-confirm" class="col-md-6 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                            <div class="col-md-12">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Register') }}
                                                </button>
                                                <button type="button" onclick="window.location='{{ Route("login") }}'" class="btn btn-primary">
                                                    {{ __('login') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--   Core JS Files   -->
@include('includes.scripts')
<!--  End Core JS Files   -->

</body>

</html>--}}
<!DOCTYPE html>
<html lang="en">

<head>
    @include('includes.header')
    <style>
        .login form{
            margin-top: 50px !important;
            background-color: #e6e5e5 !important;
        }
        body{
            background-color: white !important;
        }
    </style>
</head>

<body class="">
<div class="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 ml-auto mr-auto login form">
                <form class="text-center border border-light p-5" method="POST" action="{{ route('register') }}">
                    @csrf
                    <p class="h4 mb-4">Student Registration</p>
                    <input id="first_name" type="text"  placeholder="First Name" class="form-control mb-4 @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>

                    @error('first_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <input id="last_name" type="text"  placeholder="Last Name" class="form-control mb-4 @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                    @error('last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <input id="birth_date" onfocus="(this.type='date')" onblur="(this.type='text')" type="date"  placeholder="Birth Date" class="form-control mb-4 @error('birth_date') is-invalid @enderror" name="birth_date" value="{{ old('birth_date') }}" required autocomplete="birth_date" autofocus>

                    @error('birth_date')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <input id="nic" type="text"  placeholder="NIC" class="form-control mb-4 @error('nic') is-invalid @enderror" name="nic" value="{{ old('nic') }}" required autocomplete="nic" autofocus>

                    @error('nic')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <input id="contact_address" type="text"  placeholder="Address" class="form-control mb-4 @error('contact_address') is-invalid @enderror" name="contact_address" value="{{ old('contact_address') }}" required autocomplete="contact_address" autofocus>
                    {{--<textarea id="contact_address" type="text" placeholder="Describe yourself here..." rows="2" cols="50" class="form-control mb-4 @error('contact_address') is-invalid @enderror" required autocomplete="contact_address" autofocus>

                    </textarea>--}}
                    @error('contact_address')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <input id="guardian_name" type="text"  placeholder="Guardian Name" class="form-control mb-4 @error('guardian_name') is-invalid @enderror" name="guardian_name" value="{{ old('guardian_name') }}" required autocomplete="guardian_name" autofocus>

                    @error('guardian_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <input id="mobile_phone" type="number"  placeholder="Mobile Phone" class="form-control mb-4 @error('mobile_phone') is-invalid @enderror" name="mobile_phone" value="{{ old('mobile_phone') }}" required autocomplete="mobile_phone" autofocus>

                    @error('mobile_phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <input id="land_phone" type="number"  placeholder="Mobile Phone" class="form-control mb-4 @error('land_phone') is-invalid @enderror" name="land_phone" value="{{ old('land_phone') }}" required autocomplete="land_phone" autofocus>

                    @error('land_phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                <!-- Password -->
                    <input id="email" type="email" placeholder="E-mail" class="form-control mb-4 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    <input id="password" type="password" placeholder="Password" class="form-control mb-4 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                    <input id="password-confirm" type="password" placeholder="Password confirm" class="form-control mb-4" name="password_confirmation" required autocomplete="new-password">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                   {{-- <button type="submit" class="btn btn-info">
                        {{ __('Login') }}
                    </button>--}}
                    <a href="{{Route('login')}}" class="btn btn-info">
                        {{ __('Login') }}
                    </a>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
<!--   Core JS Files   -->
@include('includes.scripts')
<!--  End Core JS Files   -->

</body>

</html>
