@extends('layouts.app')

@section('content')
    <style>
        .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}
    </style>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Video</h4>
                    <p class="card-category">Add videos</p>
                </div>
                <div class="card-body">
                    <form action="{{Route('video.store')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">

                                <div class="">
                                    <label class="bmd-label-floating">Choose Video</label>
                                    <br>
                                    <input type="file" name="video" id="video" class="webkit-file-upload-button">
                                  {{--  <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    <input type="file" class="custom-file-input" id="inputGroupFile01"
                                           aria-describedby="inputGroupFileAddon01">--}}

                                </div>
                                <div class="progress">
                                    <div class="bar"></div >
                                    <div class="percent">0%</div >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{--<label class="bmd-label-floating">Select Class</label>--}}
                                    <select name="class" id="class" class="form-control">
                                        <option value="">Select Class</option>
                                        <option value="volvo">class 1</option>
                                        <option value="saab">class 2</option>
                                        <option value="mercedes">class 3</option>
                                        <option value="audi">class 4</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {{--<label class="bmd-label-floating">Select Month</label>--}}
                                    <select name="month" id="month" class="form-control">
                                        {{--<option value="">Select Month</option>--}}
                                        <option value="January">January</option>
                                        <option value="January">January</option>
                                        <option value="March">March</option>
                                        <option value="April">April</option>
                                        <option value="May">May</option>
                                        <option value="June">June</option>
                                        <option value="July">July</option>
                                        <option value="August">August</option>
                                        <option value="September">September</option>
                                        <option value="Octomber">Octomber</option>
                                        <option value="November">November</option>
                                        <option value="December">December</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Video Name</label>
                                    <input type="text" id="video_name" name="video_name" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Description</label>
                                    <textarea class="form-control" rows="2" required></textarea>
                                    {{--<input type="text" class="form-control">--}}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="bmd-label-floating">Is Active</label>
                                        <input type="checkbox" id="is_active" name="is_active" value="1" checked>
                                    </div>
                                </div>
                            </div>
                        </div>
                       {{-- <button type="submit" class="btn btn-primary pull-right">Update Profile</button>--}}
                        <input type="submit" class="btn btn-primary pull-right" value="Submit">
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>
    {{--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript">

        function validate(formData, jqForm, options) {
            var form = jqForm[0];
            if (!form.video.value) {
                alert('File not found');
                return false;
            }
        }

        (function() {
//debugger;
            var bar = $('.bar');
            var percent = $('.percent');
            var status = $('#status');

            $('form').ajaxForm({
                beforeSubmit: validate,
                beforeSend: function() {
                    status.empty();
                    var percentVal = '0%';
                    var posterValue = $('input[name=video]').fieldValue();
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                uploadProgress: function(event, position, total, percentComplete) {
                    var percentVal = percentComplete + '%';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                success: function() {
                    var percentVal = 'Wait, Saving';
                    bar.width(percentVal)
                    percent.html(percentVal);
                },
                complete: function(xhr) {
                    status.html(xhr.responseText);
                    //alert('Uploaded Successfully');
                    //window.location.href = "/image-upload";
                    var video_name=$('#video_name').val();
                    $.ajax.post("video",video_name)
                        .then(function successCallback(response) {
                            console.log(response);
                            // swal('Request Complete', 'Successfully Sent for Approval').done();
                            /*swal({
                                title: 'Request Complete',
                                text: 'Successfully Sent for Recommendation!',
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonColor: '#00bf86',
                                cancelButtonColor: '#ff8080',
                                confirmButtonText: 'Ok!',

                            }).then(function(){
                                location.reload();
                            });*/
                        }, function errorCallback(response) {
                            console.log(response);

                        });
                    //window.location.href = "{{Route('video.store')}}";
                }
            });

        })();
    </script>
@endsection
