<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           [
               'email'=>'pasinduchana26@gmail.com',
               'password' => Hash::make('123123'),
               'role_id'=>2,
                'name'=>'pasindu'
           ],[
                'email'=>'pasinduchana27@gmail.com',
                'password' => Hash::make('123123'),
                'role_id'=>1,
                'name'=>'pasindu chanaka'
            ]
        ]);
    }
}
